package fr.ulille.iut.pizzaland;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.Arrays;
import java.util.List;
import java.util.UUID;

import org.glassfish.jersey.test.JerseyTest;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import fr.ulille.iut.pizzaland.beans.Ingredient;
import fr.ulille.iut.pizzaland.beans.Pizza;
import fr.ulille.iut.pizzaland.dao.IngredientDao;
import fr.ulille.iut.pizzaland.dao.PizzaDao;
import fr.ulille.iut.pizzaland.dto.PizzaCreateDto;
import fr.ulille.iut.pizzaland.dto.PizzaDto;
import jakarta.ws.rs.client.Entity;
import jakarta.ws.rs.core.Application;
import jakarta.ws.rs.core.Form;
import jakarta.ws.rs.core.GenericType;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;

/*
 * JerseyTest facilite l'écriture des tests en donnant accès aux
 * méthodes de l'interface jakarta.ws.rs.client.Client.
 * la méthode configure() permet de démarrer la ressource à tester
 */

public class PizzaRessourceTest extends JerseyTest {
	private PizzaDao daoPizza;
	private IngredientDao daoIngredient;
	private List<Ingredient> ingredients = Arrays.asList(new Ingredient("Tomate"), new Ingredient("Fromage"), new Ingredient("Ananas"));

	@Override
	protected Application configure() {
		BDDFactory.setJdbiForTests();

		return new ApiV1();
	}

	@Before
	public void setEnvUp() {
		daoPizza = BDDFactory.buildDao(PizzaDao.class);
		daoPizza.createTableAndIngredientAssociation();

		daoIngredient = BDDFactory.buildDao(IngredientDao.class);
		daoIngredient.createTable();

		for (Ingredient i : ingredients) {
			daoIngredient.insert(i);
		}
	}

	@After
	public void tearEnvDown() throws Exception {
		daoPizza.dropTableAndIngredientAssociation();
		daoIngredient.dropTable();
	}

	/**
	 * Récupère toutes les pizzas dans la BDD (aucune ici)
	 */
	@Test
	public void testGetEmptyList() {
		Response response = target("/pizzas").request().get();

		// Vérification de la valeur de retour (200)
		assertEquals(Response.Status.OK.getStatusCode(), response.getStatus());

		// Vérification de la valeur retournée (liste vide)
		List<PizzaDto> pizza;
		pizza = response.readEntity(new GenericType<List<PizzaDto>>() {
		});

		assertEquals(0, pizza.size());
	}

	/**
	 * Récupère une pizza qui existe
	 */
	@Test
	public void testGetExistingPizza() {
		Pizza pizza = new Pizza("Fromage", 2, 4);
		daoPizza.insert(pizza);

		Response response = target("/pizzas").path(pizza.getId().toString()).request(MediaType.APPLICATION_JSON).get();

		assertEquals(Response.Status.OK.getStatusCode(), response.getStatus());

		Pizza result = Pizza.fromDto(response.readEntity(PizzaDto.class));
		assertEquals(pizza, result);
	}

	/**
	 * Vérifie que la récupération d'une pizza inexistante ne marche pas
	 */
	@Test
	public void testGetNotExistingPizza() {
		Response response = target("/pizzas").path(UUID.randomUUID().toString()).request().get();
		assertEquals(Response.Status.NOT_FOUND.getStatusCode(), response.getStatus());
	}

	/**
	 * Récupère une pizza avec ingrédient qui existe
	 */
	@Test
	public void testGetExistingPizzaWithIngredients() {
		Pizza pizza = new Pizza("Fromage", 2, 4, ingredients);
		daoPizza.insert(pizza);

		Response response = target("/pizzas").path(pizza.getId().toString()).request(MediaType.APPLICATION_JSON).get();
		assertEquals(Response.Status.OK.getStatusCode(), response.getStatus());

		Pizza result = Pizza.fromDto(response.readEntity(PizzaDto.class));
		assertEquals(pizza, result);
	}

	/**
	 * Vérifie que la création d'une pizza est fonctionnelle
	 */
	@Test
	public void testCreatePizza() {
		PizzaCreateDto pizzaCreateDto = new PizzaCreateDto();
		pizzaCreateDto.setName("Fromage");
		pizzaCreateDto.setSmallprice(2);
		pizzaCreateDto.setLargeprice(4);
		pizzaCreateDto.setIngredients(ingredients);

		Response response = target("/pizzas").request().post(Entity.json(pizzaCreateDto));
		assertEquals(Response.Status.CREATED.getStatusCode(), response.getStatus());

		PizzaDto returnedEntity = response.readEntity(PizzaDto.class);
		assertEquals(target("/pizzas/" + returnedEntity.getId()).getUri(), response.getLocation());

		assertEquals(returnedEntity.getName(), pizzaCreateDto.getName());
		assertEquals(returnedEntity.getSmallprice(), pizzaCreateDto.getSmallprice());
		assertEquals(returnedEntity.getLargeprice(), pizzaCreateDto.getLargeprice());
		assertEquals(returnedEntity.getIngredients(), pizzaCreateDto.getIngredients());
	}

	/**
	 * Vérifie que la création de deux pizzas avec le même nom échoue
	 */
	@Test
	public void testCreateSamePizza() {
		Pizza pizza = new Pizza("Fromage", 2, 4, ingredients);
		daoPizza.insert(pizza);

		PizzaCreateDto pizzaCreateDto = Pizza.toCreateDto(pizza);
		Response response = target("/pizzas").request().post(Entity.json(pizzaCreateDto));

		assertEquals(Response.Status.CONFLICT.getStatusCode(), response.getStatus());
	}

	/**
	 * Supprime une pizza avec ses ingrédients associés
	 */
	@Test
	public void testDeleteExistingIPizzaWithIngredient() {
		Pizza pizza = new Pizza("Fromage", 2, 4, ingredients);
		daoPizza.insert(pizza);

		Response response = target("/pizzas/").path(pizza.getId().toString()).request().delete();
		assertEquals(Response.Status.ACCEPTED.getStatusCode(), response.getStatus());

		Pizza result = daoPizza.findById(pizza.getId());
		assertEquals(result, null);
	}

	@Test
	public void testGetPizzaName() {
		Pizza pizza = new Pizza("Fromage", 2, 4);
		daoPizza.insert(pizza);

		Response response = target("/pizzas").path(pizza.getId().toString()).path("name").request().get();
		assertEquals(Response.Status.OK.getStatusCode(), response.getStatus());

		assertEquals("Fromage", response.readEntity(String.class));
	}

	@Test
	public void testGetSmallPrice() {
		Pizza pizza = new Pizza("Fromage", 2, 4);
		daoPizza.insert(pizza);

		Response response = target("/pizzas").path(pizza.getId().toString()).path("smallprice").request().get();
		assertEquals(Response.Status.OK.getStatusCode(), response.getStatus());

		assertTrue(2 == response.readEntity(int.class));
	}

	@Test
	public void testGetLargePrice() {
		Pizza pizza = new Pizza("Fromage", 2, 4);
		daoPizza.insert(pizza);

		Response response = target("/pizzas").path(pizza.getId().toString()).path("largeprice").request().get();
		assertEquals(Response.Status.OK.getStatusCode(), response.getStatus());

		assertTrue(4 == response.readEntity(int.class));
	}

	@Test
	public void testGetIngredients() {
		Pizza pizza = new Pizza("Fromage", 2, 4, ingredients);
		daoPizza.insert(pizza);

		Response response = target("/pizzas").path(pizza.getId().toString()).path("ingredients").request().get();
		assertEquals(Response.Status.OK.getStatusCode(), response.getStatus());

		assertEquals(ingredients, Ingredient.getListFromString(response.readEntity(String.class)));
	}

	@Test
	public void testCreatePizzaWithForm() {
		// On créer un formulaire
		Form form = new Form();
		form.param("name", "Fromage");
		form.param("smallprice", "2");
		form.param("largeprice", "4");
		form.param("ingredients", ingredients.get(0).getName() + "," + ingredients.get(1).getName() + "," + ingredients.get(2).getName());

		// On ajoute la pizza à l'aide des réponses du formulaire
		Entity<Form> formEntity = Entity.entity(form, MediaType.APPLICATION_FORM_URLENCODED_TYPE);
		Response response = target("pizzas").request().post(formEntity);

		assertEquals(Response.Status.CREATED.getStatusCode(), response.getStatus());
		String location = response.getHeaderString("Location");
		String id = location.substring(location.lastIndexOf('/') + 1);
		Pizza result = daoPizza.findById(UUID.fromString(id));
		assertNotNull(result);

		// On va chercher la pizza créé avec le formulaire pour verif si elle est ok
		PizzaDto returnedEntity = response.readEntity(PizzaDto.class);
		assertEquals(target("/pizzas/" + returnedEntity.getId()).getUri(), response.getLocation());

		Pizza pizza = new Pizza("Fromage", 2, 4, ingredients);
		assertEquals(returnedEntity.getName(), pizza.getName());
		assertEquals(returnedEntity.getSmallprice(), pizza.getSmallprice());
		assertEquals(returnedEntity.getLargeprice(), pizza.getLargeprice());
		assertEquals(returnedEntity.getIngredients(), pizza.getIngredients());
	}

}
