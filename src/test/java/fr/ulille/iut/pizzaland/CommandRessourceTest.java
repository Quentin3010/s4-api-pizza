package fr.ulille.iut.pizzaland;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.util.Arrays;
import java.util.List;
import java.util.UUID;

import org.glassfish.jersey.test.JerseyTest;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import fr.ulille.iut.pizzaland.beans.Commande;
import fr.ulille.iut.pizzaland.beans.Ingredient;
import fr.ulille.iut.pizzaland.beans.Pizza;
import fr.ulille.iut.pizzaland.dao.CommandeDao;
import fr.ulille.iut.pizzaland.dao.IngredientDao;
import fr.ulille.iut.pizzaland.dao.PizzaDao;
import fr.ulille.iut.pizzaland.dto.CommandeCreateDto;
import fr.ulille.iut.pizzaland.dto.CommandeDto;
import jakarta.ws.rs.client.Entity;
import jakarta.ws.rs.core.Application;
import jakarta.ws.rs.core.Form;
import jakarta.ws.rs.core.GenericType;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;

/*
 * JerseyTest facilite l'écriture des tests en donnant accès aux
 * méthodes de l'interface jakarta.ws.rs.client.Client.
 * la méthode configure() permet de démarrer la ressource à tester
 */

public class CommandRessourceTest extends JerseyTest {
	private CommandeDao daoCommande;
	private PizzaDao daoPizza;
	private IngredientDao daoIngredient;
	private List<Ingredient> ingredients = Arrays.asList(new Ingredient("Tomate"), new Ingredient("Fromage"), new Ingredient("Ananas"));
	private List<Pizza> pizzas = Arrays.asList(new Pizza("Pizza1", 1, 2, ingredients), new Pizza("Pizza2", 3, 4, ingredients));
	
	@Override
	protected Application configure() {
		BDDFactory.setJdbiForTests();

		return new ApiV1();
	}

	@Before
	public void setEnvUp() {
		daoCommande = BDDFactory.buildDao(CommandeDao.class);
		daoCommande.createTableAndIngredientAssociation();
		
		daoPizza = BDDFactory.buildDao(PizzaDao.class);
		daoPizza.createTableAndIngredientAssociation();

		daoIngredient = BDDFactory.buildDao(IngredientDao.class);
		daoIngredient.createTable();

		for (Ingredient i : ingredients) {
			daoIngredient.insert(i);
		}
		
		for (Pizza p : pizzas) {
			daoPizza.insert(p);
		}
		
		System.out.println("BEFORE EACH");
	}

	@After
	public void tearEnvDown() throws Exception {
		daoCommande.dropTableAndPizzasAssociation();
		daoPizza.dropTableAndIngredientAssociation();
		daoIngredient.dropTable();
	}


	@Test
	public void testGetEmptyList() {
		Response response = target("/commandes").request().get();

		// Vérification de la valeur de retour (200)
		assertEquals(Response.Status.OK.getStatusCode(), response.getStatus());

		// Vérification de la valeur retournée (liste vide)
		List<CommandeDto> commande;
		commande = response.readEntity(new GenericType<List<CommandeDto>>() {
		});

		assertEquals(0, commande.size());
	}
	
	@Test
	public void testGetExistingCommande() {
		Commande commande = new Commande("Robert", pizzas);
		daoCommande.insert(commande);

		Response response = target("/commandes").path(commande.getId().toString()).request(MediaType.APPLICATION_JSON).get();
		assertEquals(Response.Status.OK.getStatusCode(), response.getStatus());

		Commande result = Commande.fromDto(response.readEntity(CommandeDto.class));
		assertEquals(commande, result);
	}

	@Test
	public void testGetNotExistingCommande() {
		Response response = target("/commandes").path(UUID.randomUUID().toString()).request().get();
		assertEquals(Response.Status.NOT_FOUND.getStatusCode(), response.getStatus());
	}

	@Test
	public void testCreateCommande() {
		CommandeCreateDto commandeCreateDto = new CommandeCreateDto();
		commandeCreateDto.setName("Fromage");
		commandeCreateDto.setPizzas(pizzas);

		Response response = target("/commandes").request().post(Entity.json(commandeCreateDto));
		assertEquals(Response.Status.CREATED.getStatusCode(), response.getStatus());

		CommandeDto returnedEntity = response.readEntity(CommandeDto.class);
		assertEquals(target("/commandes/" + returnedEntity.getId()).getUri(), response.getLocation());

		assertEquals(returnedEntity.getName(), commandeCreateDto.getName());
		assertEquals(returnedEntity.getPizzas(), commandeCreateDto.getPizzas());
	}
	
	@Test
	public void testDeleteExistingCommande() {
		Commande commande = new Commande("Robert", pizzas);
		daoCommande.insert(commande);

		Response response = target("/commandes/").path(commande.getId().toString()).request().delete();
		assertEquals(Response.Status.ACCEPTED.getStatusCode(), response.getStatus());

		Pizza result = daoPizza.findById(commande.getId());
		assertEquals(result, null);
	}

	@Test
	public void testGetAllCommandesFromClientName() {
		Commande commande = new Commande("Robert", pizzas);
		daoCommande.insert(commande);
		commande = new Commande("Robert", pizzas); //obligé de recréer pour avoir un nouvel UUID
		daoCommande.insert(commande);
		commande = new Commande("Robert", pizzas); //obligé de recréer pour avoir un nouvel UUID
		daoCommande.insert(commande);

		Response response = target("/commandes").path(commande.getName()).path("name").request().get();
		assertEquals(Response.Status.OK.getStatusCode(), response.getStatus());

		assertEquals(3, (response.readEntity(List.class)).size());
	}


	@Test
	public void testCreatePizzaWithForm() {
		// On créer un formulaire
		Form form = new Form();
		form.param("name", "Robert");
		form.param("pizzas", pizzas.get(0).getName() + "," + pizzas.get(1).getName());

		// On ajoute la pizza à l'aide des réponses du formulaire
		Entity<Form> formEntity = Entity.entity(form, MediaType.APPLICATION_FORM_URLENCODED_TYPE);
		Response response = target("commandes").request().post(formEntity);
		assertEquals(Response.Status.CREATED.getStatusCode(), response.getStatus());
		
		String location = response.getHeaderString("Location");
		String id = location.substring(location.lastIndexOf('/') + 1);
		Commande result = daoCommande.findById(UUID.fromString(id));
		assertNotNull(result);

		// On va chercher la pizza créé avec le formulaire pour verif si elle est ok
		CommandeDto returnedEntity = response.readEntity(CommandeDto.class);
		assertEquals(target("/commandes/" + returnedEntity.getId()).getUri(), response.getLocation());

		Commande commande = new Commande("Robert", pizzas);
		assertEquals(returnedEntity.getName(), commande.getName());
		assertEquals(returnedEntity.getPizzas(), commande.getPizzas());
	}
}
