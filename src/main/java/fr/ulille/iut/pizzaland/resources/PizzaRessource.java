package fr.ulille.iut.pizzaland.resources;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.logging.Logger;
import java.util.stream.Collectors;

import fr.ulille.iut.pizzaland.BDDFactory;
import fr.ulille.iut.pizzaland.beans.Ingredient;
import fr.ulille.iut.pizzaland.beans.Pizza;
import fr.ulille.iut.pizzaland.dao.IngredientDao;
import fr.ulille.iut.pizzaland.dao.PizzaDao;
import fr.ulille.iut.pizzaland.dto.PizzaCreateDto;
import fr.ulille.iut.pizzaland.dto.PizzaDto;
import jakarta.ws.rs.Consumes;
import jakarta.ws.rs.DELETE;
import jakarta.ws.rs.FormParam;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.PathParam;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.WebApplicationException;
import jakarta.ws.rs.core.Context;
import jakarta.ws.rs.core.Response;
import jakarta.ws.rs.core.UriInfo;

@Produces("application/json")
@Path("/pizzas")
public class PizzaRessource {
	private static final Logger LOGGER = Logger.getLogger(PizzaRessource.class.getName());

	private PizzaDao pizzas;
	private IngredientDao ingredients;

	@Context
	public UriInfo uriInfo;

	public PizzaRessource() {
		pizzas = BDDFactory.buildDao(PizzaDao.class);
		ingredients = BDDFactory.buildDao(IngredientDao.class);
		pizzas.createTableAndIngredientAssociation();
	}

	@GET
	public List<PizzaDto> getAll() {
		LOGGER.info("PizzaRessource:getAll");

		List<PizzaDto> l = pizzas.getAll().stream().map(Pizza::toDto).collect(Collectors.toList());
		LOGGER.info(l.toString());
		return l;
	}

	@GET
	@Path("{id}")
	@Produces({ "application/json", "application/xml" })
	public PizzaDto getOnePizza(@PathParam("id") UUID id) {
		LOGGER.info("getOneFullPizza(" + id + ")");
		try {
			Pizza pizza = pizzas.findById(id);

			List<UUID> listIngredientId = pizzas.getIngredientsIdWithPizzaId(id);

			List<Ingredient> listIngredient = new ArrayList<Ingredient>();
			for (UUID uuid : listIngredientId) {
				listIngredient.add(ingredients.findById(uuid));
			}
			pizza.setIngredients(listIngredient);
			
			LOGGER.info(pizzas.toString());
			return Pizza.toDto(pizza);
		} catch (Exception e) {
			throw new WebApplicationException(Response.Status.NOT_FOUND);
		}
	}

	@GET
	@Path("{id}/name")
	@Produces({ "application/json", "application/xml" })
	public String getOnePizzaName(@PathParam("id") UUID id) {
		LOGGER.info("getOneFullPizza(" + id + ")");
		try {
			Pizza pizza = pizzas.findById(id);

			LOGGER.info(pizzas.toString());
			return pizza.getName();
		} catch (Exception e) {
			throw new WebApplicationException(Response.Status.NOT_FOUND);
		}
	}
	
	@GET
	@Path("{id}/ingredients")
	@Produces({ "application/json", "application/xml" })
	public List<Ingredient> getOnePizzaIngredients(@PathParam("id") UUID id) {
		LOGGER.info("getOneFullPizza(" + id + ")");
		try {
			List<UUID> listIngredientId = pizzas.getIngredientsIdWithPizzaId(id);
			List<Ingredient> listIngredient = new ArrayList<Ingredient>();
			for (UUID uuid : listIngredientId) {
				listIngredient.add(ingredients.findById(uuid));
			}
			
			LOGGER.info(pizzas.toString());
			return listIngredient;
		} catch (Exception e) {
			throw new WebApplicationException(Response.Status.NOT_FOUND);
		}
	}
	
	@GET
	@Path("{id}/smallprice")
	@Produces({ "application/json", "application/xml" })
	public int getOnePizzaSmallPrice(@PathParam("id") UUID id) {
		LOGGER.info("getOneFullPizza(" + id + ")");
		try {
			Pizza pizza = pizzas.findById(id);

			LOGGER.info(pizzas.toString());
			return pizza.getSmallprice();
		} catch (Exception e) {
			throw new WebApplicationException(Response.Status.NOT_FOUND);
		}
	}
	
	@GET
	@Path("{id}/largeprice")
	@Produces({ "application/json", "application/xml" })
	public int getOnePizzaLargePrice(@PathParam("id") UUID id) {
		LOGGER.info("getOneFullPizza(" + id + ")");
		try {
			Pizza pizza = pizzas.findById(id);

			LOGGER.info(pizzas.toString());
			return pizza.getLargeprice();
		} catch (Exception e) {
			throw new WebApplicationException(Response.Status.NOT_FOUND);
		}
	}
	
	@POST
	public Response createPizza(PizzaCreateDto pizzaCreateDto) {
		Pizza existing = pizzas.findByName(pizzaCreateDto.getName());
		if (existing != null) {
			throw new WebApplicationException(Response.Status.CONFLICT);
		}

		try {
			Pizza pizza = Pizza.fromPizzaCreateDto(pizzaCreateDto);
			pizzas.insert(pizza);
			PizzaDto pizzadto = Pizza.toDto(pizza);

			URI uri = uriInfo.getAbsolutePathBuilder().path(pizza.getId().toString()).build();

			return Response.created(uri).entity(pizzadto).build();
		} catch (Exception e) {
			e.printStackTrace();
			throw new WebApplicationException(Response.Status.NOT_ACCEPTABLE);
		}
	}
	
	@DELETE
	@Path("{id}")
	public Response deletePizza(@PathParam("id") UUID id) {
		if (pizzas.findById(id) == null) {
			throw new WebApplicationException(Response.Status.NOT_FOUND);
		}

		pizzas.remove(id);

		return Response.status(Response.Status.ACCEPTED).build();
	}
	
	@POST
	@Consumes("application/x-www-form-urlencoded")
	public Response createPizza(@FormParam("name") String name, @FormParam("smallprice") String smallprice, @FormParam("largeprice") String largeprice, @FormParam("ingredients") String ingredientsName) {
		Pizza existing = pizzas.findByName(name);
		if (existing != null) {
			throw new WebApplicationException(Response.Status.CONFLICT);
		}
		
		List<Ingredient> listeIngredients = new ArrayList<Ingredient>();
		for(String ingredientName : ingredientsName.split(",")) {
			listeIngredients.add(ingredients.getNameWithId(ingredientName));
		}

		try {
			Pizza pizza = new Pizza();
			pizza.setName(name);
			pizza.setSmallprice(Integer.parseInt(smallprice));
			pizza.setLargeprice(Integer.parseInt(largeprice));
			pizza.setIngredients(listeIngredients);

			pizzas.insert(pizza);

			PizzaDto pizzaDto = Pizza.toDto(pizza);

			URI uri = uriInfo.getAbsolutePathBuilder().path("" + pizza.getId()).build();

			return Response.created(uri).entity(pizzaDto).build();
		} catch (Exception e) {
			e.printStackTrace();
			throw new WebApplicationException(Response.Status.NOT_ACCEPTABLE);
		}
	}

}
