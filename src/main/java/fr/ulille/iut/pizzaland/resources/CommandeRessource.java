package fr.ulille.iut.pizzaland.resources;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.logging.Logger;
import java.util.stream.Collectors;

import fr.ulille.iut.pizzaland.BDDFactory;
import fr.ulille.iut.pizzaland.beans.Commande;
import fr.ulille.iut.pizzaland.beans.Ingredient;
import fr.ulille.iut.pizzaland.beans.Pizza;
import fr.ulille.iut.pizzaland.dao.CommandeDao;
import fr.ulille.iut.pizzaland.dao.IngredientDao;
import fr.ulille.iut.pizzaland.dao.PizzaDao;
import fr.ulille.iut.pizzaland.dto.CommandeCreateDto;
import fr.ulille.iut.pizzaland.dto.CommandeDto;
import jakarta.ws.rs.Consumes;
import jakarta.ws.rs.DELETE;
import jakarta.ws.rs.FormParam;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.PathParam;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.WebApplicationException;
import jakarta.ws.rs.core.Context;
import jakarta.ws.rs.core.Response;
import jakarta.ws.rs.core.UriInfo;

@Produces("application/json")
@Path("/commandes")
public class CommandeRessource {
	private static final Logger LOGGER = Logger.getLogger(PizzaRessource.class.getName());
	
	private CommandeDao daoCommandes;
	private PizzaDao daoPizzas;
	private IngredientDao daoIngredient;

	@Context
	public UriInfo uriInfo;

	public CommandeRessource() {
		daoPizzas = BDDFactory.buildDao(PizzaDao.class);
		daoCommandes = BDDFactory.buildDao(CommandeDao.class);
		daoIngredient = BDDFactory.buildDao(IngredientDao.class);
	}
	
	@GET
	public List<CommandeDto> getAll() {
		LOGGER.info("CommandeRessource:getAll");

		List<CommandeDto> l = daoCommandes.getAll().stream().map(Commande::toDto).collect(Collectors.toList());
		LOGGER.info(l.toString());
		return l;
	}

	@GET
	@Path("{id}")
	@Produces({ "application/json", "application/xml" })
	public CommandeDto getOneCommande(@PathParam("id") UUID id) {
		LOGGER.info("getOneCommande(" + id + ")");
		try {
			Commande commande = daoCommandes.findById(id);
			
			List<UUID> listPizzaId = daoCommandes.getPizzaIdWithCommandeId(id);
			List<Pizza> listPizza = new ArrayList<Pizza>();
			
			for (UUID uuidPizza : listPizzaId) {
				Pizza pizza = daoPizzas.findById(uuidPizza);
				
				List<UUID> pizzaIngredientId = daoPizzas.getIngredientsIdWithPizzaId(uuidPizza);
				List<Ingredient> listeIngredient = new ArrayList<Ingredient>();
				
				for (UUID uuidIngredient : pizzaIngredientId) {
					Ingredient ingredient = daoIngredient.findById(uuidIngredient);
					listeIngredient.add(ingredient);
				}
				
				pizza.setIngredients(listeIngredient);
				listPizza.add(pizza);
			}
			
			commande.setPizzas(listPizza);
			
			LOGGER.info(daoPizzas.toString());
			return Commande.toDto(commande);
		} catch (Exception e) {
			throw new WebApplicationException(Response.Status.NOT_FOUND);
		}
	}

	@GET
	@Path("{name}/name")
	@Produces({ "application/json", "application/xml" })
	public List<Commande> getAllCommandesWithClientName(@PathParam("name") String name) {
		LOGGER.info("getAllCommandesWithClientName(" + name + ")");
		try {
			
			List<UUID> listCommandesId = daoCommandes.getAllCommandesFromClientName(name);
			List<Commande> listeCommandes = new ArrayList<Commande>();
			for(UUID id : listCommandesId) {
				listeCommandes.add(daoCommandes.findById(id));
			}

			LOGGER.info(listeCommandes.toString());
			return listeCommandes;
		} catch (Exception e) {
			throw new WebApplicationException(Response.Status.NOT_FOUND);
		}
	}

	@POST
	public Response createCommande(CommandeCreateDto commandeCreateDto) {
		try {
			Commande commande = Commande.fromCommandeCreateDto(commandeCreateDto);
			daoCommandes.insert(commande);
			CommandeDto commandedto = Commande.toDto(commande);

			URI uri = uriInfo.getAbsolutePathBuilder().path(commande.getId().toString()).build();

			return Response.created(uri).entity(commandedto).build();
		} catch (Exception e) {
			e.printStackTrace();
			throw new WebApplicationException(Response.Status.NOT_ACCEPTABLE);
		}
	}

	@DELETE
	@Path("{id}")
	public Response deleteCommande(@PathParam("id") UUID id) {
		if (daoCommandes.findById(id) == null) {
			throw new WebApplicationException(Response.Status.NOT_FOUND);
		}

		daoCommandes.remove(id);

		return Response.status(Response.Status.ACCEPTED).build();
	}
	
	@POST
	@Consumes("application/x-www-form-urlencoded")
	public Response createPizza(@FormParam("name") String name, @FormParam("pizzas") String pizzasName) {
		List<Pizza> listePizzas = new ArrayList<Pizza>();
		for(String pizzaName : pizzasName.split(",")) {
			System.out.println("PIZAAA NAME -> " + pizzaName);
			Pizza pizza = daoPizzas.findByName(pizzaName);
			
			List<Ingredient> listeIngredients = new ArrayList<Ingredient>();
			for(UUID id : daoPizzas.getIngredientsIdWithPizzaId(pizza.getId())) {
				System.out.println("  INGREDIENT -> " + id);
				Ingredient ingredient = daoIngredient.findById(id);
				listeIngredients.add(ingredient);
			}
			
			pizza.setIngredients(listeIngredients);
			listePizzas.add(pizza);
		}

		try {
			Commande commande = new Commande();
			commande.setName(name);
			commande.setPizzas(listePizzas);

			daoCommandes.insert(commande);

			CommandeDto commandeDto = Commande.toDto(commande);

			URI uri = uriInfo.getAbsolutePathBuilder().path("" + commande.getId()).build();

			return Response.created(uri).entity(commandeDto).build();
		} catch (Exception e) {
			e.printStackTrace();
			throw new WebApplicationException(Response.Status.NOT_ACCEPTABLE);
		}
	}
	
}
