package fr.ulille.iut.pizzaland.dao;

import java.util.List;
import java.util.UUID;

import org.jdbi.v3.sqlobject.config.RegisterBeanMapper;
import org.jdbi.v3.sqlobject.customizer.Bind;
import org.jdbi.v3.sqlobject.customizer.BindBean;
import org.jdbi.v3.sqlobject.statement.SqlQuery;
import org.jdbi.v3.sqlobject.statement.SqlUpdate;
import org.jdbi.v3.sqlobject.transaction.Transaction;

import fr.ulille.iut.pizzaland.beans.Commande;
import fr.ulille.iut.pizzaland.beans.Pizza;

public interface CommandeDao {
	
	@SqlUpdate("CREATE TABLE IF NOT EXISTS Commandes (id VARCHAR(128) PRIMARY KEY, name VARCHAR(128))")
	void createCommandeTable();

	@SqlUpdate("CREATE TABLE IF NOT EXISTS CommandesPizzasAssociation (id_commande VARCHAR(128), id_pizza VARCHAR(128))")
	void createAssociationTable();
	
	@Transaction
	default void createTableAndIngredientAssociation() {
		createAssociationTable();
		createCommandeTable();
	}
	
	@SqlUpdate("DROP TABLE IF EXISTS Commandes")
    void dropCommandeTable();
	
	@SqlUpdate("DROP TABLE IF EXISTS CommandesPizzasAssociation")
    void dropAssociationTable();
	
	@Transaction
	default void dropTableAndPizzasAssociation() {
		dropCommandeTable();
		dropAssociationTable();
	}
	
	@SqlUpdate("INSERT INTO Commandes(id, name) VALUES (:id, :name)")
    void insertCommande(@BindBean Commande commande);

	@SqlQuery("SELECT * FROM Commandes")
    @RegisterBeanMapper(Commande.class)
    List<Commande> getAll();

	@SqlQuery("SELECT * FROM Commandes WHERE id = :id")
    @RegisterBeanMapper(Commande.class)
    Commande findById(@Bind("id") UUID id);
	
	@SqlUpdate("INSERT INTO CommandesPizzasAssociation(id_commande, id_pizza) VALUES (:id_commande, :id_pizza)")
    void associatePizzaWithCommande(@Bind("id_commande") UUID id_commande, @Bind("id_pizza") UUID id_pizza);

	@Transaction
	default void insert(Commande commande) {
		insertCommande(commande);
		for(Pizza p : commande.getPizzas()) {
			associatePizzaWithCommande(commande.getId(), p.getId());
		}
	}
	
	@SqlQuery("SELECT id_pizza FROM CommandesPizzasAssociation WHERE id_commande = :id_commande")
    List<UUID> getPizzaIdWithCommandeId(@Bind("id_commande") UUID id_commande);
	
	@SqlUpdate("DELETE FROM Commandes WHERE id = :id")
    void removeCommande(@Bind("id") UUID id);
	
	@SqlUpdate("DELETE FROM CommandesPizzasAssociation WHERE id_commande = :id_commande")
    void removeCommandePizza(@Bind("id_commande") UUID id_commande);

	@Transaction
	default void remove(UUID id) {
		removeCommande(id);
		removeCommandePizza(id);
	}

	@SqlQuery("SELECT id FROM Commandes WHERE name = :name")
	List<UUID> getAllCommandesFromClientName(@Bind("name") String name);

	/*
	@SqlQuery("SELECT * FROM pizzas WHERE name = :name")
    @RegisterBeanMapper(Pizza.class)
    Pizza findByName(@Bind("name") String name);
    */
}
