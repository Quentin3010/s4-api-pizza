package fr.ulille.iut.pizzaland.dao;

import java.util.List;
import java.util.UUID;

import org.jdbi.v3.sqlobject.config.RegisterBeanMapper;
import org.jdbi.v3.sqlobject.customizer.Bind;
import org.jdbi.v3.sqlobject.customizer.BindBean;
import org.jdbi.v3.sqlobject.statement.SqlQuery;
import org.jdbi.v3.sqlobject.statement.SqlUpdate;
import org.jdbi.v3.sqlobject.transaction.Transaction;

import fr.ulille.iut.pizzaland.beans.Ingredient;
import fr.ulille.iut.pizzaland.beans.Pizza;

public interface PizzaDao {

	@SqlUpdate("CREATE TABLE IF NOT EXISTS Pizzas (id VARCHAR(128) PRIMARY KEY, name VARCHAR, smallprice INT, largeprice INT)")
	void createPizzaTable();

	@SqlUpdate("CREATE TABLE IF NOT EXISTS PizzaIngredientsAssociation (id_pizza VARCHAR(128), id_ingredient VARCHAR(128))")
	void createAssociationTable();
	
	@Transaction
	default void createTableAndIngredientAssociation() {
		createAssociationTable();
		createPizzaTable();
	}
	
	@SqlUpdate("DROP TABLE IF EXISTS Pizzas")
    void dropPizzaTable();
	
	@SqlUpdate("DROP TABLE IF EXISTS PizzaIngredientsAssociation")
    void dropAssociationTable();
	
	@Transaction
	default void dropTableAndIngredientAssociation() {
		dropPizzaTable();
		dropAssociationTable();
	}
	
	@SqlUpdate("INSERT INTO Pizzas(id, name, smallprice, largeprice) VALUES (:id, :name, :smallprice, :largeprice)")
    void insertPizza(@BindBean Pizza pizza);

	@SqlQuery("SELECT * FROM Pizzas")
    @RegisterBeanMapper(Pizza.class)
    List<Pizza> getAll();

	@SqlQuery("SELECT * FROM Pizzas WHERE id = :id")
    @RegisterBeanMapper(Pizza.class)
    Pizza findById(@Bind("id") UUID id);
	
	@SqlUpdate("INSERT INTO PizzaIngredientsAssociation(id_pizza, id_ingredient) VALUES (:id_pizza, :id_ingredient)")
    void associateIngtredientWithPizza(@Bind("id_pizza") UUID id_pizza, @Bind("id_ingredient") UUID id_ingredient);

	@Transaction
	default void insert(Pizza pizza) {
		insertPizza(pizza);
		for(Ingredient i : pizza.getIngredients()) {
			associateIngtredientWithPizza(pizza.getId(), i.getId());
		}
	}
	
	@SqlQuery("SELECT id_ingredient FROM PizzaIngredientsAssociation WHERE id_pizza = :id_pizza")
    List<UUID> getIngredientsIdWithPizzaId(@Bind("id_pizza") UUID id_pizza);
	
	@SqlUpdate("DELETE FROM Pizzas WHERE id = :id")
    void removePizza(@Bind("id") UUID id);
	
	@SqlUpdate("DELETE FROM PizzaIngredientsAssociation WHERE id_pizza = :id_pizza")
    void removePizzaIngredient(@Bind("id_pizza") UUID id_pizza);

	@Transaction
	default void remove(UUID id) {
		removePizza(id);
		removePizzaIngredient(id);
	}

	@SqlQuery("SELECT * FROM pizzas WHERE name = :name")
    @RegisterBeanMapper(Pizza.class)
    Pizza findByName(@Bind("name") String name);
}