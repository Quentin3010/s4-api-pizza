package fr.ulille.iut.pizzaland.beans;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import fr.ulille.iut.pizzaland.dto.PizzaCreateDto;
import fr.ulille.iut.pizzaland.dto.PizzaDto;

public class Pizza {
	private UUID id = UUID.randomUUID();
	private String name;
	private int smallprice;
	private int largeprice;
	private List<Ingredient> ingredients = new ArrayList<Ingredient>();

	public Pizza() {
	}

	public Pizza(String name, int smallprice, int largeprice) {
		this(name, smallprice, largeprice, new ArrayList<Ingredient>());
	}

	public Pizza(String name, int smallprice, int largeprice, List<Ingredient> ingredients) {
		this.name = name;
		this.smallprice = smallprice;
		this.largeprice = largeprice;
		this.ingredients = ingredients;
	}

	public UUID getId() {
		return id;
	}

	public void setId(UUID id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getSmallprice() {
		return smallprice;
	}

	public void setSmallprice(int smallprice) {
		this.smallprice = smallprice;
	}

	public int getLargeprice() {
		return largeprice;
	}

	public void setLargeprice(int largeprice) {
		this.largeprice = largeprice;
	}

	public List<Ingredient> getIngredients() {
		return ingredients;
	}

	public void setIngredients(List<Ingredient> ingredients) {
		this.ingredients = ingredients;
	}

	public static PizzaDto toDto(Pizza p) {
		PizzaDto dto = new PizzaDto();
		dto.setId(p.getId());
		dto.setName(p.getName());
		dto.setSmallprice(p.getSmallprice());
		dto.setLargeprice(p.getLargeprice());
		dto.setIngredients(p.ingredients);

		return dto;
	}

	public static Pizza fromDto(PizzaDto dto) {
		Pizza pizza = new Pizza();
		pizza.setId(dto.getId());
		pizza.setName(dto.getName());
		pizza.setSmallprice(dto.getSmallprice());
		pizza.setLargeprice(dto.getLargeprice());
		pizza.setIngredients(dto.getIngredients());

		return pizza;
	}

	@Override
	public String toString() {
		return "Pizza [id=" + id + ", name=" + name + ", smallprice=" + smallprice + ", largeprice=" + largeprice
				+ ", ingredients=" + ingredients + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((ingredients == null) ? 0 : ingredients.hashCode());
		result = prime * result + largeprice;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + smallprice;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Pizza other = (Pizza) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (ingredients == null) {
			if (other.ingredients != null)
				return false;
		} else if (!ingredients.equals(other.ingredients))
			return false;
		if (largeprice != other.largeprice)
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (smallprice != other.smallprice)
			return false;
		return true;
	}
	
	public static PizzaCreateDto toCreateDto(Pizza pizza) {
		PizzaCreateDto dto = new PizzaCreateDto();
		dto.setName(pizza.getName());
		dto.setIngredients(pizza.getIngredients());
		dto.setLargeprice(pizza.getLargeprice());
		dto.setSmallprice(pizza.getSmallprice());

		return dto;
	}

	public static Pizza fromPizzaCreateDto(PizzaCreateDto dto) {
		Pizza ingredient = new Pizza();
		ingredient.setName(dto.getName());
		ingredient.setIngredients(dto.getIngredients());
		ingredient.setLargeprice(dto.getLargeprice());
		ingredient.setSmallprice(dto.getSmallprice());

		return ingredient;
	}

}
