package fr.ulille.iut.pizzaland.dto;

import java.util.ArrayList;
import java.util.List;

import fr.ulille.iut.pizzaland.beans.Ingredient;

public class PizzaCreateDto {
	private String name;
	private int smallprice;
	private int largeprice;
	private List<Ingredient> ingredients = new ArrayList<Ingredient>(); 

	public PizzaCreateDto() {
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getSmallprice() {
		return smallprice;
	}

	public void setSmallprice(int smallprice) {
		this.smallprice = smallprice;
	}

	public int getLargeprice() {
		return largeprice;
	}

	public void setLargeprice(int largeprice) {
		this.largeprice = largeprice;
	}

	public List<Ingredient> getIngredients() {
		return ingredients;
	}

	public void setIngredients(List<Ingredient> ingredients) {
		this.ingredients = ingredients;
	}

	
}
