package fr.ulille.iut.pizzaland.dto;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import fr.ulille.iut.pizzaland.beans.Pizza;

public class CommandeDto {
	private UUID id;
	private String name;
	private List<Pizza> pizzas = new ArrayList<Pizza>();

	public CommandeDto() {
	}

	public CommandeDto(UUID id, String name, List<Pizza> pizzas) {
		this.id = id;
		this.name = name;
		this.pizzas = pizzas;
	}

	public UUID getId() {
		return id;
	}

	public void setId(UUID id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<Pizza> getPizzas() {
		return pizzas;
	}

	public void setPizzas(List<Pizza> pizzas) {
		this.pizzas = pizzas;
	}
	
}
