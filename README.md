# API PIZZA

Quentin BERNARD

## Description du projet

Le projet "API Pizza" est une API REST conçue en Java afin de faciliter la commande de pizzas. Comme son nom l'indique, cette API offre un ensemble de fonctionnalités pour créer, supprimer et lister des ingrédients, des pizzas et des commandes. 

Réalisé dans le cadre d'un projet de fin de semestre en programmation répartie, ce projet met en œuvre les principes de conception d'une API REST. Il vise à offrir une interface conviviale et flexible pour la gestion des pizzas, permettant aux utilisateurs de personnaliser leurs commandes et de profiter d'une expérience de commande fluide et agréable.

Vidéo de présentation : lien

## Fonctionnalités

- Récupérer la liste de toutes les pizzas disponibles : GET /pizzas
- Récupérer les détails d'une pizza spécifique : GET /pizzas/{id}
- Récupérer le nom d'une pizza spécifique : GET /pizzas/{id}/name
- Récupérer la liste des ingrédients d'une pizza spécifique : GET /pizzas/{id}/ingredients
- Récupérer le prix pour le format mini d'une pizza spécifique : GET /pizzas/{id}/smallprice
- Récupérer le prix pour le format large d'une pizza spécifique : GET /pizzas/{id}/largeprice
- Ajouter une nouvelle pizza : POST /pizzas (avec les détails de la pizza dans la requête)
- Supprimer une pizza spécifique : DELETE /pizzas/{id}
- Récupérer la liste de toutes les commandes : GET /commandes
- Récupérer les détails d'une commande spécifique : GET /commandes/{id}
- Récupérer la liste des commandes d'un client (avec son nom) : GET /commandes/{name}/name
- Ajouter une nouvelle commande : POST /commandes (avec les détails de la commande dans la requête)
- Supprimer une commande spécifique : DELETE /commandes/{id}

## Requêtes

| URI | Opération | MIME | Requête | Réponse | 
| ----------------------- | ---------- | --------------------------------------------- | -- | ---------------------------------------------------- |
| /pizzas | GET | <-application/json  <-application/xml | | liste des pizzas (I2) | 
| /pizzas/{id} | GET | <-application/json  <-application/xml | | une pizza (I2) ou 404 | 
| /pizzas/{id}/name | GET | <-text/plain | | le nom de la pizza ou 404 | 
| /pizzas/{id}/ingredients | GET | <-text/plain | | la liste des ingrédients de la pizza ou 404 |
| /pizzas/{id}/smallprice | GET | <-text/plain | | le prix pour le format mini de la pizza ou 404 | 
| /pizzas/{id}/largeprice | GET | <-text/plain | | la prix  pour le format large de la pizza ou 404 | 
| /pizzas | POST | <-/->application/json  ->application/x-www-form-urlencoded | Pizza (I1) | Nouvel pizza (I2)  409 si la pizza existe déjà (même nom) | 
| /pizzas/{id} | DELETE | | | |

| URI | Opération | MIME | Requête | Réponse | 
| ----------------------- | ---------- | --------------------------------------------- | -- | ---------------------------------------------------- |
| /commandes | GET | <-application/json  <-application/xml | | liste des commandes (I2) | 
| /commandes/{id} | GET | <-application/json  <-application/xml | | une commande (I2) ou 404 | 
| /commandes/{name}/name| GET | <-text/plain | | la liste des commandes d'un client (avec son nom) ou 404 | 
| /commandes | POST | <-/->application/json  ->application/x-www-form-urlencoded | Commande (I1) | Nouvel commande (I2)  409 si la commande existe déjà (même nom) | 
| /commandes/{id} | DELETE | | | |

